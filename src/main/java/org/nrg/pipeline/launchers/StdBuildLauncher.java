/*
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 *
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.utils.PipelineUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.model.XnatMrsessiondataI;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;

public class StdBuildLauncher extends PipelineLauncher{

	public static final String  NAME = "StdBuild.xml";
	public static final String  LOCATION = "build-tools";
	public static final	String STDBUILDTEMPLATE = "PipelineScreen_StdBuild.vm";

	public static final	String MPRAGE = "MPRAGE";
	public static final String MPRAGE_PARAM = "mprs";
	
	
	public static final	String T2 = "T2";
	
	public static final String T2_PARAM = "t2";
	public static final	String EPI = "BOLD";
	
	public static final String EPI_PARAM = "fstd";
	public static final	String DTI = "DTI";
	
	public static final String DTI_PARAM = "dti";
	public static final	String FLAIR = "FLAIR";


	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(StdBuildLauncher.class);

	public boolean launch(RunData data, Context context) {
		boolean rtn = false;
		try {
		ItemI data_item = TurbineUtils.GetItemBySearch(data);
		XnatMrsessiondata mr = new XnatMrsessiondata(data_item);
		XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, mr);
		String pipelineName = data.getParameters().get("stdbuild_pipelinename");
		String cmdPrefix = data.getParameters().get("cmdprefix");
		xnatPipelineLauncher.setPipelineName(pipelineName);
		String buildDir = PipelineFileUtils.getBuildDir(mr.getProject(), true);
		buildDir +=   "stdb"  ;
		xnatPipelineLauncher.setBuildDir(buildDir);
		xnatPipelineLauncher.setNeedsBuildDir(false);
		
		Parameters parameters = Parameters.Factory.newInstance();
		
//		xnatPipelineLauncher.setParameter("isDicom", data.getParameters().get("isDicom"));
        ParameterData param = parameters.addNewParameter();
    	param.setName("isDicom");
    	param.addNewValues().setUnique(data.getParameters().get("isDicom"));

        param = parameters.addNewParameter();
    	param.setName("rm_prev_folder");
    	param.addNewValues().setUnique("0");
		
//		xnatPipelineLauncher.setParameter("sessionId",mr.getLabel());
		param = parameters.addNewParameter();
    	param.setName("sessionId");
    	param.addNewValues().setUnique(mr.getLabel());

		boolean build = false;
		//xnatPipelineLauncher.setParameter("archivedir", mr.getArchivePath());

		String target = data.getParameters().get("target");
//		xnatPipelineLauncher.setParameter("target",target);
		param = parameters.addNewParameter();
    	param.setName("target");
    	param.addNewValues().setUnique(target);


//		xnatPipelineLauncher.setParameter("tr_vol",data.getParameters().get("tr_vol"));
 /*   	if (TurbineUtils.HasPassedParameter("tr_vol", data)) {
    	param = parameters.addNewParameter();
    	param.setName("tr_vol");
    	param.addNewValues().setUnique(data.getParameters().get("tr_vol"));
    	}
    	
//		xnatPipelineLauncher.setParameter("skip",data.getParameters().get("skip"));
    	if (TurbineUtils.HasPassedParameter("skip", data)) {
    	param = parameters.addNewParameter();
    	param.setName("skip");
    	param.addNewValues().setUnique(data.getParameters().get("skip"));
    	}
    */	
		if (TurbineUtils.HasPassedParameter(PipelineUtils.GRAD_UNWARP, data)) {
			boolean grad_unwarp = data.getParameters().getBoolean(PipelineUtils.GRAD_UNWARP);
//			xnatPipelineLauncher.setParameter("grad_unwarp",grad_unwarp?"1":"0");
			param = parameters.addNewParameter();
	    	param.setName("grad_unwarp");
	    	param.addNewValues().setUnique(grad_unwarp?"1":"0");
		}else {
//			xnatPipelineLauncher.setParameter("grad_unwarp","0");
			param = parameters.addNewParameter();
	    	param.setName("grad_unwarp");
	    	param.addNewValues().setUnique("0");
		}

		if (TurbineUtils.HasPassedParameter(PipelineUtils.CROSS_DAY_REGISTER, data)) {
			String cross_day_register = data.getParameters().get(PipelineUtils.CROSS_DAY_REGISTER);
			if (!cross_day_register.equals("-1")) {
				XnatMrsessiondataI crossMr = XnatMrsessiondata.getXnatMrsessiondatasById(cross_day_register, TurbineUtils.getUser(data), false);
				if (crossMr != null)
//				   xnatPipelineLauncher.setParameter("cross_day", ((XnatMrsessiondata)crossMr).getArchivePath()+File.separator+crossMr.getLabel());
					param = parameters.addNewParameter();
				   param.setName("cross_day");
	    	       param.addNewValues().setUnique(((XnatMrsessiondata)crossMr).getArchivePath()+File.separator+crossMr.getLabel());

					param = parameters.addNewParameter();
				   param.setName("cross_day_xnat_id");
	    	       param.addNewValues().setUnique(crossMr.getId());

	    	       
//					xnatPipelineLauncher.setParameter("sessionRef", ((XnatMrsessiondata)crossMr).getLabel());
					param = parameters.addNewParameter();
				    param.setName("sessionRef");
		    	    param.addNewValues().setUnique(((XnatMrsessiondata)crossMr).getLabel());

//				xnatPipelineLauncher.setParameter("sessionRefPath", ((XnatMrsessiondata)crossMr).getArchivePath()+crossMr.getLabel());
				param = parameters.addNewParameter();
			    param.setName("sessionRefPath");
	    	    param.addNewValues().setUnique(((XnatMrsessiondata)crossMr).getArchivePath()+crossMr.getLabel());

			}
		}


		ArrayList<String> mprs = getCheckBoxSelections(data,mr,StdBuildLauncher.MPRAGE);
//		ArrayList<String> t2s = getCheckBoxSelections(data,mr,StdBuildLauncher.T2);
//		ArrayList<String> bold = getCheckBoxSelections(data,mr,StdBuildLauncher.EPI);

		if (TurbineUtils.HasPassedParameter("build_" + StdBuildLauncher.MPRAGE, data)) {
//			xnatPipelineLauncher.setParameter("mprs",mprs);
		      param = parameters.addNewParameter();
		      param.setName("mprs");
		      Values values = param.addNewValues();
		      if (mprs.size() == 1) {
		    	  values.setUnique(mprs.get(0));
		      }else {
			       for (int i = 0; i < mprs.size(); i++) {
			        	values.addList(mprs.get(i));
			        }
		      }
			build = true;
		}
/*		if (TurbineUtils.HasPassedParameter("build_" + StdBuildLauncher.T2, data)) {
//			xnatPipelineLauncher.setParameter("t2",t2s);
		      param = parameters.addNewParameter();
		      param.setName("t2");
		      Values values = param.addNewValues();
		      if (t2s.size() == 1) {
		    	  values.setUnique(t2s.get(0));
		      }else {
			      for (int i = 0; i < t2s.size(); i++) {
			        	values.addList(t2s.get(i));
			        }
		      }
			if (TurbineUtils.HasPassedParameter(PipelineUtils.COLLATE, data)) {
//	    		xnatPipelineLauncher.setParameter(PipelineUtils.COLLATE,"1");
				param = parameters.addNewParameter();
		    	param.setName(PipelineUtils.COLLATE);
		    	param.addNewValues().setUnique("1");
			}
			build = true;
		}

		if (TurbineUtils.HasPassedParameter("build_" + StdBuildLauncher.EPI, data)) {
//			xnatPipelineLauncher.setParameter("fstd",bold);
		      param = parameters.addNewParameter();
		      param.setName("fstd");
		      Values values = param.addNewValues();
		      if (bold.size() == 1) {
		    	  values.setUnique(bold.get(0));
		      }else {
			      for (int i = 0; i < bold.size(); i++) {
			        	values.addList(bold.get(i));
			        }
		      }

			if (bold.size() > 0) {
				ArrayList<String> irun = getRunLabels(bold);
//	    		xnatPipelineLauncher.setParameter("irun",irun);
			      param = parameters.addNewParameter();
			      param.setName("irun");
			       values = param.addNewValues();
			      if (irun.size() == 1) {
			    	  values.setUnique(irun.get(0));
			      }else {
				      for (int i = 0; i < irun.size(); i++) {
				        	values.addList(irun.get(i));
				        }
			      }

				String epidir = data.getParameters().get("epidir");
//	    		xnatPipelineLauncher.setParameter("epidir",epidir);
				param = parameters.addNewParameter();
		    	param.setName("epidir");
		    	param.addNewValues().setUnique(epidir);
		    	
				String epi2atl = data.getParameters().get("epi2atl");
				param = parameters.addNewParameter();
		    	param.setName("epi2atl");
		    	param.addNewValues().setUnique(epi2atl);

				String normode = data.getParameters().get("normode");
				param = parameters.addNewParameter();
		    	param.setName("normode");
		    	param.addNewValues().setUnique(normode);

				String economy = data.getParameters().get("economy");
				param = parameters.addNewParameter();
		    	param.setName("economy");
		    	param.addNewValues().setUnique(economy);


				ArrayList<String> functionalScans = getCheckBoxSelections(data,mr,"functional_"+StdBuildLauncher.EPI, data.getParameters().getInt(StdBuildLauncher.EPI+"_rowcount"));
	            if (functionalScans.size() > 0) {
	            	ArrayList<String> functional = getRunLabels(bold,irun,functionalScans);
//	        		xnatPipelineLauncher.setParameter("fcbolds",functional);
				      param = parameters.addNewParameter();
				      param.setName("fcbolds");
				       values = param.addNewValues();
				      if (functional.size() == 1) {
				    	  values.setUnique(functional.get(0));
				      }else {
					      for (int i = 0; i < functional.size(); i++) {
					        	values.addList(functional.get(i));
					        }
				      }
//	        		xnatPipelineLauncher.setParameter("preprocessFunctional","1");
					param = parameters.addNewParameter();
			    	param.setName("preprocessFunctional");
			    	param.addNewValues().setUnique("1");

	            }
			}
			build = true;
		}

		ArrayList<String> dti = getCheckBoxSelections(data,mr,StdBuildLauncher.DTI);
		if (TurbineUtils.HasPassedParameter("build_" + StdBuildLauncher.DTI, data)) {
//			xnatPipelineLauncher.setParameter("dti",dti);
		      param = parameters.addNewParameter();
		      param.setName("dti");
		      Values  values = param.addNewValues();
		      if (dti.size() == 1) {
		    	  values.setUnique(dti.get(0));
		      }else {
			      for (int i = 0; i < dti.size(); i++) {
			        	values.addList(dti.get(i));
			        }
		      }
			//DTI Build Parameters:
//			xnatPipelineLauncher.setParameter("bVec",data.getParameters().get("bvec"));
				param = parameters.addNewParameter();
		    	param.setName("bVec");
		    	param.addNewValues().setUnique(data.getParameters().get("bvec"));

//		      xnatPipelineLauncher.setParameter("ibmax",data.getParameters().get("ibmax"));
				param = parameters.addNewParameter();
		    	param.setName("ibmax");
		    	param.addNewValues().setUnique(data.getParameters().get("ibmax"));

//		    	xnatPipelineLauncher.setParameter("r",data.getParameters().get("resolution"));
				param = parameters.addNewParameter();
		    	param.setName("r");
		    	param.addNewValues().setUnique(data.getParameters().get("resolution"));

		    	build = true;
		}
*/
		if (build) {
			String paramFileName = getName(pipelineName);
			Date date = new Date();
		    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		    String s = formatter.format(date);
			
			paramFileName += "_params_" + s + ".xml"; 

			String paramFilePath = saveParameters(buildDir+File.separator + mr.getLabel(),paramFileName,parameters);  
		    xnatPipelineLauncher.setParameterFile(paramFilePath);
			rtn = xnatPipelineLauncher.launch(cmdPrefix);
		}else rtn = true;

		if (TurbineUtils.HasPassedParameter(PipelineUtils.FREESURFER, data)) {
			//boolean freesurfer = data.getParameters().getBoolean(PipelineUtils.FREESURFER);
			//if (freesurfer) {
				boolean frsfrSuccess =  new FreesurferLauncher(mprs).launch(data, context, mr);
				rtn = rtn && frsfrSuccess;
			//}
		}

		}catch(Exception e) {
			logger.debug(e);
		}
		return rtn;
	}

	private ArrayList<String> getRunLabels(ArrayList<String> boldScans) {
		ArrayList<String> rtn = new ArrayList<String>();
		for (int i = 0; i < boldScans.size(); i++) {
			rtn.add("run"+(i+1));
		}
		return rtn;
	}

	private ArrayList<String> getRunLabels(ArrayList<String> boldScans,ArrayList<String> runLabels,ArrayList<String> functionalScans ) {
		ArrayList<String> rtn = new ArrayList<String>();
		for (int i = 0; i < functionalScans.size(); i++) {
			for (int j=0; j<boldScans.size(); j++) {
				if (boldScans.get(j).equals(functionalScans.get(i))) {
					rtn.add(runLabels.get(j));
					break;
				}
			}
		}
		return rtn;
	}


}
