
package org.nrg.xnat.turbine.modules.screens;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.launchers.StdBuildLauncher;
import org.nrg.xdat.om.ArcPipelineparameterdata;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;

import java.io.File;
import java.util.LinkedHashMap;

public class PipelineScreen_Freesurfer extends DefaultPipelineScreen {

    String mprageScanType;
    static Logger logger = Logger.getLogger(PipelineScreen_Freesurfer.class);
    public void finalProcessing(RunData data, Context context){
        try {
            XnatMrsessiondata mr = (XnatMrsessiondata) om;
            context.put("mr", mr);

            ArcPipelineparameterdata  mprageParam = getProjectPipelineSetting(StdBuildLauncher.MPRAGE_PARAM);
            if (mprageParam != null) {
                mprageScanType = mprageParam.getCsvvalues();
                setHasDicomFiles(mr, mprageScanType, context);
            }
            String selfStatus = WrkWorkflowdata.GetLatestWorkFlowStatusByPipeline(mr.getId(), XnatMrsessiondata.SCHEMA_ELEMENT_NAME, StdBuildLauncher.LOCATION+File.separator +StdBuildLauncher.NAME, mr.getProject(), TurbineUtils.getUser(data));
            if (selfStatus.equalsIgnoreCase(WrkWorkflowdata.COMPLETE)) {
                data.setMessage("This pipeline has already completed. Relaunching the pipeline may result in loss of processed files");
            }
            context.put("projectSettings", projectParameters);
            LinkedHashMap<String,String> buildableScanTypes = new LinkedHashMap<String,String>();
            if (mprageScanType != null)
                buildableScanTypes.put(StdBuildLauncher.MPRAGE, mprageScanType);
            context.put("buildableScanTypes", buildableScanTypes);
        } catch(Exception e) {
            logger.error("Possibly the project wide pipeline has not been set", e);
            e.printStackTrace();
        }
    }
}
